package br.com.thiagoft.modularapp.controllers;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.thiagoft.modularapp.dao.interfaces.UserDAO;
import br.com.thiagoft.modularapp.model.entities.User;

@RestController
@Transactional
public class UserController {
	
	@Autowired
	public UserDAO userDAO;
	
	@RequestMapping("/save")
	public void save() {
		User user = new User();
		user.setName("Thaisa");
		userDAO.insert(user);
	}
}
