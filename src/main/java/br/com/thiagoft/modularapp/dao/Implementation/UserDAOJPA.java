package br.com.thiagoft.modularapp.dao.Implementation;

import org.springframework.stereotype.Repository;

import br.com.thiagoft.modularapp.dao.interfaces.UserDAO;
import br.com.thiagoft.modularapp.model.entities.User;

@Repository
public class UserDAOJPA extends AbstractDAOJPA<User> implements UserDAO {

}
