package br.com.thiagoft.modularapp.dao.interfaces;

import java.util.List;

public interface BaseDAO<T> {
	
	public void insert(T obj);
	public void update(T obj);
	public void remove(T obj);
	public List<T> list();
	
}
