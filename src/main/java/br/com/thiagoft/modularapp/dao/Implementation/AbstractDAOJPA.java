package br.com.thiagoft.modularapp.dao.Implementation;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class AbstractDAOJPA<T> {
	
	@PersistenceContext
	public EntityManager manager;
	
	public void insert(T obj) {
		manager.persist(obj);
	}

	public void update(T obj) {
		manager.merge(obj);
	}

	public void remove(T obj) {
		manager.remove(obj);		
	}

	public List<T> list() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
