package br.com.thiagoft.modularapp.dao.interfaces;

import br.com.thiagoft.modularapp.model.entities.Notification;

public interface NotificationDAO extends BaseDAO<Notification> {

}
