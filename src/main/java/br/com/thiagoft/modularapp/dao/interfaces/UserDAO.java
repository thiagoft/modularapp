package br.com.thiagoft.modularapp.dao.interfaces;

import br.com.thiagoft.modularapp.model.entities.User;

public interface UserDAO extends BaseDAO<User> {

}
