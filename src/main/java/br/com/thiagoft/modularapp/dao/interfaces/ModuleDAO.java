package br.com.thiagoft.modularapp.dao.interfaces;

import br.com.thiagoft.modularapp.model.entities.Module;

public interface ModuleDAO extends BaseDAO<Module> {

}
