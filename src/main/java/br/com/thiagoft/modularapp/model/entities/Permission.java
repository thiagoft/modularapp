package br.com.thiagoft.modularapp.model.entities;

public class Permission {
	
	private Long id;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Permission(Long id) {
		this.id = id;
	}
	
	public Permission() {}
	
}
