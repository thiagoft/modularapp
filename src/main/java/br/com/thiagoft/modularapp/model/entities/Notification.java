package br.com.thiagoft.modularapp.model.entities;

public class Notification {

	private Long id;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Notification(Long id) {
		super();
		this.id = id;
	}
	
	public Notification() {}
}
