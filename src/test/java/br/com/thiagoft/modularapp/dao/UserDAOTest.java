package br.com.thiagoft.modularapp.dao;

import javax.transaction.Transactional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.thiagoft.modularapp.dao.interfaces.UserDAO;
import br.com.thiagoft.modularapp.model.entities.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy(value = { 
		@ContextConfiguration("classpath:spring-servlet.xml")
})
public class UserDAOTest {
	
	@Autowired
	private UserDAO userDAO;
	
	@Test
	@Transactional
	public void testaInsercao() {
		User user = new User();
		user.setName("Teste");
		userDAO.insert(user);
		Assert.assertTrue(user.getId() != null);
	}
	
}
